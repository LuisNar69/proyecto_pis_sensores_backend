'use strict';
var models = require('../models')
var catalogo = models.catalogo;

class CatalogoControl {

    async listar(req, res) {
        var lista = await catalogo.findAll({
            attributes: ['nombre', 'rango_minimo', "rango_maximo", "informacion"]
        });
        //codigos de respuesta http
        res.status(200);
        res.json({ msg: "OK", code: 200, datos: lista });
    }

    async guardar(req, res) {
        if (req.body.hasOwnProperty('nombre') &&
            req.body.hasOwnProperty('minimo') &&
            req.body.hasOwnProperty('maximo') &&
            req.body.hasOwnProperty('info')) {
            var uuid = require('uuid');
            var data = {
                nombre: req.body.nombre,
                rango_minimo: req.body.minimo,
                rango_maximo: req.body.maximo,
                informacion: req.body.info,
                external_id: uuid.v4()
            }
            //create permite guardar datos en bd
            var result = await catalogo.create(data);
            if (result === null) {
                res.status(200);
                res.json({ msg: "ERROR", tag: "No se puede crear", code: 401 });

            } else {
                res.status(200);
                res.json({ msg: "OK", code: 200 });

            }
        } else {
            res.status(400);
            res.json({ msg: "ERROR", tag: "Faltan datos", code: 400 });
        }


    }

}

module.exports = CatalogoControl;