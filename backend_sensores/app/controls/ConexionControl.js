'use strict';
var models = require('../models');
const schedule = require('node-schedule');
const axios = require('axios');

var sensor = models.sensor;
var esp = models.espMaestro;
var datoRecolectado = models.datoRecolectado;

const obtenerFechaActual = function () {
    const fechaActual = new Date();
    const dia = fechaActual.getDate();
    const mes = fechaActual.getMonth() + 1;
    const anio = fechaActual.getFullYear();

    // Formatear la fecha como "YYYY-MM-DD"
    const fechaFormateada = `${anio}-${mes.toString().padStart(2, '0')}-${dia.toString().padStart(2, '0')}`;
    return fechaFormateada;
};

const obtenerHoraActual = function () {
    const ahora = new Date();

    const horas = ahora.getHours().toString().padStart(2, '0');
    const minutos = ahora.getMinutes().toString().padStart(2, '0');

    return `${horas}:${minutos}`;
}


const configurarJob = async function () {

    var esp32 = await esp.findOne();

    if (esp32 !== null) {

        const minutoInicial = new Date().getMinutes();
        //const cronExpression = `${minutoInicial} * * * *`; //horas
        //const cronExpression = `*/10 * * * * *`;// segundos
        const cronExpression = ` */10 * * * *`;



        const job = schedule.scheduleJob(cronExpression, async function () {
            console.log(minutoInicial);
            var lista = await sensor.findAll();
            var ipEsp = await esp.findOne();


            for (const elemento of lista) {
                if (elemento.estado === true) {
                    //DIRECCION DEL ESP32 SERVIDOR
                    const esp32URL = `http://${ipEsp.ip}/sensor?valor=${elemento.ip}`;
                    //console.log(esp32URL);
                    try {
                        const esp32Response = await fetch(esp32URL, { method: 'GET', timeout: 2000 });
                        const esp32Data = await esp32Response.json();
                        console.log(elemento.ip);
                        if (esp32Response.ok) {
                            const datoSensor = esp32Data.dato;
                            console.log(datoSensor);
                            var uuid = require('uuid');

                            let transaction = await models.sequelize.transaction();

                            try {
                                var data = {
                                    dato: datoSensor,
                                    hora: obtenerHoraActual(),
                                    fecha: obtenerFechaActual(),
                                    id_sensor: elemento.id,
                                    external_id: uuid.v4(),
                                }

                                var result = await datoRecolectado.create(data, { transaction });

                                await transaction.commit();

                                if (result === null) {
                                    console.log("no se pudo guardar correctamente")
                                } else {
                                    console.log("guardado.correctamente")
                                }

                            } catch (error) {
                                if (transaction) await transaction.rollback();
                                console.error("error")
                            }

                        } else {
                            console.error("Error en la respuesta del servidor:", esp32Response.statusText);
                            console.error("respuesta:", esp32Data.msg);
                        }

                    } catch (error) {
                        console.error("Error al realizar la solicitud:", error.message);
                    }
                };
            };
        });
    } else {
        console.log("No se pudo obtener el maestro desde la base de datos. El job no se configurará.");
    }
}

configurarJob();

class ConexionControl {

    /*Listar los sensores y sus datos */
    async listar(req, res) {
        let fechaHoy = obtenerFechaActual();
        var lista = await sensor.findAll({
            attributes: ["nombre", "ubicacion", "tipo_sensor", ["external_id", "id"]],
            include: [{
                model: models.datoRecolectado, as: "datoRecolectado",
                where: { fecha: fechaHoy },
                attributes: ["dato", 'fecha', 'hora']
            }]
        });
        res.status(200);
        res.json({ msg: "OK", code: 200, datos: lista });
    }

}

module.exports = ConexionControl;
