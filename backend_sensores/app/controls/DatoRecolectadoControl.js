'use strict';
var models = require('../models/');
var sensor = models.sensor;
var datoRecolectado = models.datoRecolectado;

const obtenerFechaActual = function () {
    const fechaActual = new Date();
    const dia = fechaActual.getDate();
    const mes = fechaActual.getMonth() + 1;
    const anio = fechaActual.getFullYear();

    // Formatear la fecha como "YYYY-MM-DD"
    const fechaFormateada = `${anio}-${mes.toString().padStart(2, '0')}-${dia.toString().padStart(2, '0')}`;
    return fechaFormateada;
};

class datoRecolectadoControl {

    /*Listar los sensores y sus datos */
    async listar(req, res) {
        let fechaHoy = obtenerFechaActual();
        var lista = await sensor.findAll({
            attributes: ["nombre", "ubicacion", "tipo_sensor", ["external_id", "id"]],
            include: [{
                model: models.datoRecolectado, as: "datoRecolectado",
                where: { fecha: fechaHoy },
                attributes: ["dato", 'fecha', 'hora']
            }]
        });
        res.status(200);
        res.json({ tag: "OK", code: 200, datos: lista });
    }

    /*Listar los sensores y sus datos */
    async obtenerTemperatura(req, res) {
        var lista = await sensor.findOne({
            attributes: ["nombre", "tipo_sensor"],
            where: { tipo_sensor: "TEMPERATURA" },
            include: [{
                model: models.datoRecolectado,
                as: "datoRecolectado",
                where: {},
                order: [['fecha', 'DESC']],
                limit: 1,
                attributes: ["dato", 'fecha']
            }]
        });
        res.status(200);
        res.json({ tag: "OK", code: 200, datos: lista });
    }

    /*Busqueda y listado de sensores y datos por fecha */
    async listarFecha(req, res) {
        let parametro = req.params.valorBusqueda;
        var lista = await sensor.findAll({
            attributes: ["nombre", "ubicacion", "tipo_sensor", ["external_id", "id"]],
            include: [{
                model: models.datoRecolectado, as: "datoRecolectado",
                where: { fecha: parametro },
                attributes: ["dato", 'fecha', 'hora']
            }]
        });
        res.status(200);
        res.json({ tag: "OK", code: 200, datos: lista });
    }

    /*Busqueda y listado de sensores y datos por tipo de sensor */
    async listarTipo(req, res) {
        let parametro = req.params.valorBusqueda;
        var lista = await sensor.findAll({
            where: { tipo_Sensor: parametro },
            attributes: ["nombre", "ubicacion", "tipo_sensor", ["external_id", "id"]],
            include: [{
                model: models.datoRecolectado, as: "datoRecolectado",
                attributes: ["dato", 'fecha', 'hora']
            }]
        });
        res.status(200);
        res.json({ tag: "OK", code: 200, datos: lista });
    }

    /*Busqueda y listado de sensores y datos por nombre de sensor */
    async listarNombre(req, res) {
        let parametro = req.params.valorBusqueda;
        var lista = await sensor.findAll({
            where: { nombre: parametro },
            attributes: ["nombre", "ubicacion", "tipo_sensor", ["external_id", "id"]],
            include: [{
                model: models.datoRecolectado, as: "datoRecolectado",
                attributes: ["dato", 'fecha', 'hora']
            }]
        });
        res.status(200);
        res.json({ tag: "OK", code: 200, datos: lista });
    }

}

module.exports = datoRecolectadoControl;