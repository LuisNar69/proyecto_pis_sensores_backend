'use strict';
var models = require('../models')
var esp32 = models.espMaestro;

const expresionRegularIP = /^(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)$/;

class Esp32Control {
    //para listar
    async listar(req, res) {
        var lista = await esp32.findAll({
            attributes: ['ip', 'external_id']
        });

        //codigos de respuesta http
        res.status(200);
        res.json({ msg: "OK", code: 200, datos: lista });
    }

    async guardar(req, res) {
        console.log("ENTRO");
        if (req.body.hasOwnProperty('ip')) {

            var uuid = require('uuid');

            if (expresionRegularIP.test(req.body.ip)) {

                if (req.body.external === undefined) {
                    var data = {
                        ip: req.body.ip,
                        external_id: uuid.v4()
                    }
                    var result = await esp32.create(data);
                } else {

                    var espAux = await esp32.findOne({ where: { external_id: req.body.external } });

                    if (espAux) {
                        espAux.ip = req.body.ip;
                        espAux.external_id = uuid.v4();
                        var result = await espAux.save();
                    }
                }

                if (result === null) {
                    res.status(200);
                    res.json({ msg: "ERROR", tag: "No se puede guardar", code: 401 });
                } else {
                    res.status(200);
                    res.json({ msg: "OK", code: 200 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Error", tag: "direccion ip incorrecta", code: 400 });
            }

        } else {
            res.status(400);
            res.json({ msg: "ERROR", tag: "Faltan datos", code: 400 });
        }
    }
}

module.exports = Esp32Control;