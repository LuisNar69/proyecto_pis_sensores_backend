'use strict';
var models = require('../models')
var persona = models.persona;
var rol = models.rol;
var cuenta = models.cuenta;
const bcrypt = require('bcrypt');
const saltRounds = 8;

class PersonaControl {
    //para listar personas
    async listarP(req, res) {
        var lista = await persona.findAll({
            include: [
                { model: models.cuenta, as: "cuenta", attributes: ['correo', 'estado'] },
                { model: models.rol, as: "rol", attributes: ['nombre', 'external_id'] },
            ],
            attributes: ['apellidos', 'external_id', 'nombres', 'telefono', 'cedula']

        });
        //codigos de respuesta http
        res.status(200);
        res.json({ msg: "OK", code: 200, datos: lista });
    }
    //obtener por external
    async obtenerP(req, res) {
        const external = req.params.external;
        var lista = await persona.findOne({
            where: { external_id: external },
            include: [
                { model: models.cuenta, as: "cuenta", attributes: ['correo', 'estado'] },
                { model: models.rol, as: "rol", attributes: ['nombre', 'external_id'] },
            ],
            attributes: ['apellidos', 'external_id', 'nombres', 'telefono', 'cedula']
        });
        if (lista === undefined || lista == null) {
            //codigos de respuesta http
            res.status(200);
            res.json({ tag: "OK", code: 200, datos: {} });

        } else {
            //codigos de respuesta http
            res.status(200);
            res.json({ tag: "OK", code: 200, datos: lista });
        }
    }

    /*Busqueda y listado de personas por cedula */
    async obtenerCedula(req, res) {
        let parametro = req.params.valorBusqueda;
        var lista = await persona.findAll({
            where: { cedula: parametro },
            include: [
                { model: models.cuenta, as: "cuenta", attributes: ['correo', 'estado'] },
                { model: models.rol, as: "rol", attributes: ['nombre', 'external_id'] },
            ],
            attributes: ['apellidos', 'external_id', 'nombres', 'telefono', 'cedula']
        });
        res.status(200);
        res.json({ tag: "OK", code: 200, datos: lista });
    }

    /*Busqueda y listado de personas por nombre */
    async obtenerNombre(req, res) {
        let parametro = req.params.valorBusqueda;
        var lista = await persona.findAll({
            where: { nombres: parametro },
            include: [
                { model: models.cuenta, as: "cuenta", attributes: ['correo', 'estado'] },
                { model: models.rol, as: "rol", attributes: ['nombre', 'external_id'] },
            ],
            attributes: ['apellidos', 'external_id', 'nombres', 'telefono', 'cedula']
        });
        res.status(200);
        res.json({ tag: "OK", code: 200, datos: lista });
    }

    //guardar datos de una persona
    async guardarP(req, res) {

        if (req.body.hasOwnProperty('nombres') &&
            req.body.hasOwnProperty('apellidos') &&
            req.body.hasOwnProperty('telefono') &&
            req.body.hasOwnProperty('cedula') &&
            req.body.hasOwnProperty('correo') &&
            req.body.hasOwnProperty('clave') &&
            req.body.hasOwnProperty('rol')) {

            var uuid = require('uuid');
            var rolAux = await rol.findOne({ where: { external_id: req.body.rol } });
            if (rolAux != undefined) {
                // Validar el formato de la cédula usando una expresión regular
                const cedulaRegex = /^[0-9]{10}$/; // Ejemplo: Cédula de 10 dígitos
                var claveHash = function (clave) {
                    return bcrypt.hashSync(clave, bcrypt.genSaltSync(saltRounds), null);//cifrar clave
                };
                if (!cedulaRegex.test(req.body.cedula)) {
                    res.status(400);
                    res.json({ msg: "ERROR", tag: "Formato de cédula inválido", code: 400 });
                    return; // Detener la ejecución si la cédula no tiene el formato correcto
                }

                var data = {
                    nombres: req.body.nombres,
                    apellidos: req.body.apellidos,
                    telefono: req.body.telefono,
                    cedula: req.body.cedula,
                    id_rol: rolAux.id,
                    external_id: uuid.v4(),
                    cuenta: {
                        correo: req.body.correo,
                        clave: claveHash(req.body.clave),
                    }
                }
                let transaction = await models.sequelize.transaction();
                try {
                    var result = await persona.create(data, { include: [{ model: models.cuenta, as: "cuenta" }], transaction });
                    await transaction.commit();
                    if (result === null) {
                        res.status(401);
                        res.json({ msg: "ERROR", tag: "No se puede crear", code: 401 });
                    } else {
                        rolAux.external_id = uuid.v4();
                        await rolAux.save();
                        res.status(200);
                        res.json({ tag: "OK", code: 200 });
                    }
                } catch (error) {
                    if (transaction) await transaction.rollback();
                    res.status(203);
                    res.json({ tag: "Error", code: 203, error_msg: error });
                }

            } else {
                res.status(400);
                res.json({ msg: "ERROR", tag: "El dato no existe", code: 400 });
            }

        } else {
            res.status(400);
            res.json({ msg: "ERROR", tag: "Aun faltan datos", code: 400 });
        }
    }

    /*Modificar los datos de una persona*/
    async modificarP(req, res) {

        var perAux = await persona.findOne({
            where: { external_id: req.params.external },
            include: [
                { model: models.cuenta, as: "cuenta", attributes: ['external_id'] },
            ],
        });

        if (perAux === null || perAux === undefined) {
            res.status(400);
            res.json({ msg: "Error", tag: "El dato a modificar no existe", code: 400 });
        } else {

            if (req.body.hasOwnProperty('nombres') &&
                req.body.hasOwnProperty('apellidos') &&
                req.body.hasOwnProperty('telefono') &&
                req.body.hasOwnProperty('cedula')) {

                var claveHash = function (clave) {
                    return bcrypt.hashSync(clave, bcrypt.genSaltSync(saltRounds), null);//cifrar clave
                };

                var rolAux = await rol.findOne({ where: { external_id: req.body.rol } });
                perAux.id_rol = rolAux.id;

                perAux.nombres = req.body.nombres;
                perAux.apellidos = req.body.apellidos;
                perAux.telefono = req.body.telefono;
                perAux.cedula = req.body.cedula;
                var result2;

                if (req.body.clave !== '') {
                    var cuentaAux = await cuenta.findOne({ where: { external_id: perAux.cuenta.external_id } });
                    cuentaAux.clave = claveHash(req.body.clave);
                    result2 = await cuentaAux.save();
                }

                if (req.body.correo !== '') {
                    var cuentaAux = await cuenta.findOne({ where: { external_id: perAux.cuenta.external_id } });
                    cuentaAux.correo = req.body.correo;
                    result2 = await cuentaAux.save();
                }

                var result = await perAux.save();

                if (result === null || result2 === null) {
                    res.status(400);
                    res.json({ msg: "Error", tag: "No se han modificado los datos", code: 400 });
                } else {
                    res.status(200);
                    res.json({ msg: "Success", tag: "Datos modificados correctamente", code: 200 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Error", tag: "faltan datos", code: 400 });
            }
        }
    }

    /*Dar de baja los datos de un sensor*/
    async cambiarEstado(req, res) {

        var perAux = await persona.findOne({
            where: { external_id: req.params.external },
            include: [
                { model: models.cuenta, as: "cuenta", attributes: ['external_id', "estado"] },
            ],
        });

        if (perAux === null || perAux === undefined) {
            res.status(400);
            res.json({ msg: "Error", tag: "El dato a modificar no existe", code: 400 });
        } else {
            var uuid = require('uuid');
            var cuentaAux = await cuenta.findOne({ where: { external_id: perAux.cuenta.external_id } });
            if (req.body.estado === "true") {
                cuentaAux.estado = true;
            } else {
                cuentaAux.estado = false;
            }
            cuentaAux.external_id = uuid.v4();
            perAux.external_id = uuid.v4();
            var result = await cuentaAux.save();
            var result2 = await perAux.save();

            if (result === null && result2 === null) {
                res.status(400);
                res.json({ msg: "Error", tag: "No se pudo dar de baja la cuenta", code: 400 });
            } else {
                if (req.body.estado === "true") {
                    res.status(200);
                    res.json({ msg: "Success", tag: "Cuenta activada correctamente", code: 200 });
                } else {
                    res.status(200);
                    res.json({ msg: "Success", tag: "Cuenta dada de baja correctamente", code: 200 });
                }
            }
        }

    }
}


module.exports = PersonaControl;