'use strict';
var models = require('../models');
const fs = require('fs');
var sensor = models.sensor;

const expresionRegularIP = /^(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)\.(25[0-5]|2[0-4][0-9]|[0-1]?[0-9][0-9]?)$/;

class SensorControl {

    /*Listar los sensores guardados*/
    async listar(req, res) {
        var lista = await sensor.findAll({
            attributes: ['nombre', 'external_id', 'ubicacion', 'tipo_sensor', 'estado', 'img', 'ip'],
        });
        res.status(200);
        res.json({ tag: "OK", code: 200, datos: lista });
    }

    /*Busqueda y listado de sensores por tipo de sensor */
    async obtenerTipo(req, res) {
        let parametro = req.params.valorBusqueda;
        var lista = await sensor.findAll({
            where: { tipo_Sensor: parametro },
            attributes: ['nombre', 'external_id', 'ubicacion', 'tipo_sensor', 'estado', 'img', 'ip'],
        });
        res.status(200);
        res.json({ tap: "OK", code: 200, datos: lista });
    }

    /*Busqueda y listado de sensores  por nombre de sensor */
    async obtenerNombre(req, res) {
        let parametro = req.params.valorBusqueda;
        var lista = await sensor.findAll({
            where: { nombre: parametro },
            attributes: ['nombre', 'external_id', 'ubicacion', 'tipo_sensor', 'estado', 'img', 'ip'],
        });
        res.status(200);
        res.json({ tag: "OK", code: 200, datos: lista });
    }

    /*Busqueda de un sensor*/
    async obtener(req, res) {
        const external = req.params.external;
        var lista = await sensor.findOne({
            where: { external_id: external },
            attributes: ['nombre', 'external_id', 'ubicacion', 'tipo_sensor', 'estado', 'img', 'ip'],
        });
        if (lista === null) {
            lista = {};
        }
        res.status(200);
        res.json({ tag: "OK!", code: 200, datos: lista });
    }

    /*Guardar datos de un sensor*/
    async guardar(req, res) {

        const senData = JSON.parse(req.body.sen);

        if (senData.hasOwnProperty('nombre') &&
            senData.hasOwnProperty('ip') &&
            senData.hasOwnProperty('ubicacion') &&
            senData.hasOwnProperty('tipo')) {

            var uuid = require('uuid');

            if (expresionRegularIP.test(senData.ip)) {

                var data = {
                    nombre: senData.nombre,
                    ip: senData.ip,
                    external_id: uuid.v4(),
                    ubicacion: senData.ubicacion,
                    tipo_sensor: senData.tipo,
                }

                let transaction = await models.sequelize.transaction();

                try {

                    const aux = await sensor.create(data, { transaction });
                    await transaction.commit();

                    // Verifica si se han cargado archivos de imagen
                    if (req.files && req.files['img']) {
                        const filesImg = req.files['img'];
                        for (const file of filesImg) {
                            const imageUrl = `http://localhost:3006/multimedia/${file.filename}`;
                            aux.img = imageUrl;
                            await aux.save();
                        }
                    }

                    res.status(200).json({ msg: 'OK', tag: 'Sensor registrado correctamente', code: 200 });

                } catch (error) {
                    await transaction.rollback();
                    res.status(203);
                    console.log(error)
                    res.json({ tag: "Error", code: 203, error_msg: error });
                }
            } else {
                res.status(400);
                res.json({ msg: "Error", tag: "direccion ip incorrecta", code: 400 });
            }
        } else {
            res.status(400);
            res.json({ msg: "ERROR", tag: "Faltan datos", code: 400 });
        }

    }

    /*Modificar los datos de un sensor*/
    async modificar(req, res) {

        var doc = await sensor.findOne({ where: { external_id: req.params.external } })
        const senData = JSON.parse(req.body.sen);

        if (doc === null || doc === undefined) {
            res.status(400);
            res.json({ msg: "Error", tag: "El dato a modificar no existe", code: 400 });
        } else {

            if (senData.hasOwnProperty('nombre') &&
                senData.hasOwnProperty('ip') &&
                senData.hasOwnProperty('ubicacion') &&
                senData.hasOwnProperty('tipo')) {

                var uuid = require('uuid')

                if (expresionRegularIP.test(senData.ip)) {
                    const filesImg = req.files['img'];
                    let url = "";

                    if (doc.img != "" && doc.img != null) {
                        url = doc.img.split("/").pop();
                    }

                    if (filesImg != null || filesImg != undefined) {
                        if (filesImg[0].filename != url) {
                            fs.unlink('public/multimedia/' + url, (err) => {
                                if (err) {
                                    console.error('Error al intentar eliminar el archivo:', err);
                                } else {
                                    console.log('Archivo eliminado correctamente.');
                                }
                            });
                        }
                        doc.img = `http://localhost:3006/multimedia/${filesImg[0].filename}`;
                    }

                    doc.nombre = senData.nombre;
                    doc.ubicacion = senData.ubicacion;
                    doc.ip = senData.ip;
                    doc.tipo_sensor = senData.tipo;
                    doc.external_id = uuid.v4();

                    var result = await doc.save();

                    if (result === null) {
                        res.status(400);
                        res.json({ msg: "Error", tag: "No se han modificado los datos", code: 400 });
                    } else {
                        res.status(200);
                        res.json({ msg: "Success", tag: "Datos modificados correctamente", code: 200 });
                    }
                } else {
                    res.status(400);
                    res.json({ msg: "Error", tag: "direccion ip incorrecta", code: 400 });
                }
            } else {
                res.status(400);
                res.json({ msg: "Error", tag: "faltan datos", code: 400 });
            }
        }
    }

    /*Activar o desactivar un sensor*/
    async cambiarEstado(req, res) {

        var sen = await sensor.findOne({ where: { external_id: req.params.external } });
        var uuid = require('uuid');
        sen.external_id = uuid.v4();

        if (req.body.estado === "true") {
            sen.estado = true;
        } else {
            sen.estado = false;
        }
        var result = await sen.save();

        if (result === null) {
            res.status(400);
            res.json({ msg: "Error", tag: "No se ha modificado el estado", code: 400 });
        } else {
            res.status(200);
            res.json({ msg: "Success", tag: "Estado cambiado correctamente", code: 200 });
        }
    }

}

module.exports = SensorControl;