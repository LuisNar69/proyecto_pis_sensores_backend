'use strict'

module.exports = (sequelize, DataTypes) => {

    const motaClimatica = sequelize.define('motaClimatica', {
        external_id: { type: DataTypes.UUID, defaultValue: DataTypes.UUIDV4 }

    }, { freezeTableName: true });
    motaClimatica.associate = function (models) {
        motaClimatica.belongsTo(models.sensor, { foreignKey: 'id_sensor' });
        motaClimatica.belongsTo(models.persona, { foreignKey: 'id_persona' });
    }
    return motaClimatica;
}